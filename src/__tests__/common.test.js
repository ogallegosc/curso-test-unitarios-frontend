import {
    getProducts,
    getStockProduct
} from '../commonMatchers';

describe('suite de test unitarios', () => {
    test('traer lista de productos', () => {
        const listaProductos = getProducts();
        expect(listaProductos).toContain('televisor')
    })

    test('probar un numero', () => {
        expect(getStockProduct()).toBeGreaterThan(1)
    })
})