import React from 'react';
import Footer from '../components/footer';
import { mount, shallow } from 'enzyme';

describe('suite test componente', () => {
    test('footer', () => {
        const wrapper = mount(<Footer />);
        expect(wrapper.find('#titulo').text()).toEqual('Curso Test Unitarios')
    })

    test('con shallow', () => {
        shallow(<Footer />);
    })
})