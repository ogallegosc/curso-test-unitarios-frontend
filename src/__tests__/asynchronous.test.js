import { callToApi } from '../asynchronous';

describe('suite de test asincrono', () => {
    test('llamada a un a api', async () => {
        const data = await callToApi();
        expect(data).toBe('success');
    })
})