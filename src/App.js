import React from 'react';
import './App.css';
import Product from './components/product';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Mini Ripley
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Product />
      </header>
    </div>
  );
}

export default App;
