import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import imgProduct from '../product.jpg'

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function SimpleCard() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
            Samsung
        </Typography>
        <Typography variant="h5" component="h2">
            ASPIRADORA ROBOT SAMSUNG
        </Typography>
        <img src={imgProduct} alt={'sku'} />
        <Typography className={classes.pos} color="textSecondary">
            VR05R5050WK/ZS
        </Typography>
        <Typography variant="body2" component="p">
          <br />
          {'SKU: 2000378714361P'}
        </Typography>
      </CardContent>
      <CardActions>
        <Button variant="contained" color="secondary" size="small">Comprar</Button>
      </CardActions>
    </Card>
  );
}
