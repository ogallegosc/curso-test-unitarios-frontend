import React from 'react';

const Footer = () => (
  <div id="container">
    <p id="titulo">Curso Test Unitarios</p>
    <p>Todos los derechos reservados</p>
  </div>
);

export default Footer;